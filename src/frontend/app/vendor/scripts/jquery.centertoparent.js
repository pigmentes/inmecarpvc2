/*
 * Plugin Name: Resize Image to Parent Container
 *
 * Author: Christian Varga
 * Author URI: http://christianvarga.com
 * Plugin Source: https://github.com/levymetal/jquery-resize-image-to-parent/
 *
 */

(function($) {
  $.fn.centerToParent = function(opts) {
    var defaults = {
     parent: 'div'
    }

    var opts = $.extend(defaults, opts);

    function positionImage(obj) {
      // reset image (in case we're calling this a second time, for example on resize)
      obj.css({'margin-left': ''});

      // dimensions of the parent
      var parentWidth = obj.parents(opts.parent).width();     

      // dimensions of the image
      var imageWidth = obj.width();      

      // step 1 - calculate the percentage difference between image width and container width
      var diff = imageWidth / parentWidth;     

      // step 2 - center image in container
      var leftOffset = (imageWidth - parentWidth) / -2;      
      
      obj.css({'margin-left': leftOffset});    
    }

    return this.each(function() {
      var obj = $(this);

      // hack to force ie to run the load function... ridiculous bug 
      // http://stackoverflow.com/questions/7137737/ie9-problems-with-jquery-load-event-not-firing
      obj.attr("src", obj.attr("src"));

      // bind to load of image
      obj.load(function() {
        positionImage(obj);
      });

      // run the position function if the image is cached
      if (this.complete) {
        positionImage(obj);
      }

      // run the position function on window resize (to make it responsive)
      $(window).on('resize', function() {
        positionImage(obj);
      });
    });
  }
})( jQuery );