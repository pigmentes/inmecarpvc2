$(function () {

	$('.center > img').centerToParent();
	$('.expand > img').resizeToParent();

	$('.responsive').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  centerMode: false,
	  variableWidth: true,
	  prevArrow: $('.prev'),
      nextArrow: $('.next'),
	});	


	$('#nav-wrapper').height($("#nav").height());
    
    $('#nav').affix({
        offset: { top: $('#nav').offset().top }
    });

    $('.nav a').on('click', function(){ 
        if($('.navbar-toggle').css('display') !='none'){
            $(".navbar-toggle").trigger( "click" );
        }
    });
})
