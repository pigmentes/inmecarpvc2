server {
    server_name {{ server_names | join(" ") }};

    # no security problem here, since / is alway passed to upstream
    root {{ base_path }};

    # serve directly - analogous for static/staticfiles
    location /media/ {
        expires 1y;
        alias {{ media_path }};

        # if asset versioning is used
        if ($query_string) {
            expires max;
        }
    }
    location /static/ {
        expires 1y;
        autoindex on;
        alias {{ static_path }};
    }
    location / {
        client_max_body_size 10M;
        proxy_pass_header Server;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_connect_timeout 10;
        proxy_read_timeout 10;
        proxy_pass http://{{ gunicorn_socket }};
    }
    # what to serve if upstream is not available or crashes
    error_page 500 502 503 504 /static/50x.html;
}
