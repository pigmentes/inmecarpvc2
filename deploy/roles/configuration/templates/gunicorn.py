bind = '{{ gunicorn_socket }}'
errorlog = '{{ log_path }}/gunicorn.error'
accesslog = '{{ log_path }}/gunicorn.access'
