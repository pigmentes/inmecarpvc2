# -*- coding: utf-8 -*-
DEBUG = True

SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
)

MANAGERS = (
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': 'gustavodg',
        'PASSWORD': 'gustavo123',
        'HOST': '10.3.0.95',
        'PORT': '',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
