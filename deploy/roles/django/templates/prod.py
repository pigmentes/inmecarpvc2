# -*- coding: utf-8 -*-
# Django settings for {{ project_name }} project.

# Make this unique, and don't share it with anybody. / apg -m 64
SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
)

MANAGERS = (
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
    (u'Yamile Coronel', 'yamile@pigmentes.com'),
    (u'Daniel Torres', 'daniel@pigmentes.com'),
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
)

DEBUG = False

ALLOWED_HOSTS = [
{% for server_name in server_names %}
    '{{ server_name }}',
{% endfor %}
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': '{{ project_name }}',
        'PASSWORD': '{{ db_pass }}',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

DBBACKUP_FILESYSTEM_DIRECTORY = '{{ backup_path }}'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
STATIC_ROOT = '{{ static_path }}'
MEDIA_ROOT = '{{ media_path }}'

# Locale
LOCALE_PATHS = (
    '{{ locale_path }}',
)
