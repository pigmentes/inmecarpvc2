# -*- coding: utf-8 -*-
DEBUG = True

SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    (u'Daniel Torres', 'daniel@pigmentes.com'),
)

MANAGERS = (
    (u'Daniel Torres', 'daniel@pigmentes.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
